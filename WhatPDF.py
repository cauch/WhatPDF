#!/usr/bin/env python3

#    WhatPDF is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    WhatPDF is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with WhatPDF.  If not, see <http://www.gnu.org/licenses/>.

import popplerqt4
from PyQt4 import QtGui,QtCore
import sys

class Sequence:
    def __init__(self,links=[]):
        self.list_tB = []
        self.links = links
        self.comparison_link = None
        self.comparison_score = 0

    def __lt__(self, other):
        if (self.top() != other.top()):
            return self.top() < other.top()
        if (self.left() != other.left()):
            return self.left() < other.left()
        return self

    def add(self,tb,links=None):
        self.list_tB.append(tb)

        if "link" not in dir(tb):
            tb.link = None

        links2 = []
        if links:
            if type(links) != type([]):
                links2 = [links]
            else:
                links2 = links
        for l in links2:
            if l not in self.links:
                self.links.append(l)

    def text(self):
        text = " ".join([x.text() for x in self.list_tB])
        return text

    def left(self):
        if self.list_tB == []:
            return 0
        return self.list_tB[0].boundingBox().left()

    def top(self):
        if self.list_tB == []:
            return 0
        return self.list_tB[0].boundingBox().top()

    def associate(self,seq):
        text1 = []
        tBli1 = []
        cut = 0
        for x in range(len(self.list_tB)):
            text = self.list_tB[x].text()
            if cut == 1:
                cut = 0
                tBli1[-1].append(x)
                text1[-1] += " "+text
                text1[-1] = text1[-1].replace("- ","")
            else:
                tBli1.append([x])
                text1.append(text)
            if text.endswith("-"):
                cut =1
        text2 = []
        tBli2 = []
        cut = 0
        for x in range(len(seq.list_tB)):
            text = seq.list_tB[x].text()
            if cut == 1:
                cut = 0
                tBli2[-1].append(x)
                text2[-1] += " "+text
                text2[-1] = text2[-1].replace("- ","")
            else:
                tBli2.append([x])
                text2.append(text)
            if text.endswith("-"):
                cut =1
        current_index1 = -1
        current_index2 = -1
        while 1:
            if len(text1) == 0 or len(text2) == 0:
                break
            if current_index1 == -1:
                #1) find the longest word
                texts = [[len(x),x] for x in text1]
                texts.sort()
                word = texts[-1][1]
                if word in text2:
                    current_index1 = text1.index(word)
                    current_index2 = text2.index(word)
                else:
                    index1 = text1.index(word)
                    text1 = text1[:index1]+text1[index1+1:]
                    tBli1 = tBli1[:index1]+tBli1[index1+1:]
                    continue
            #2) check if current_index1 and current_index2 are the same
            find = 0
            if current_index1 < len(text1) and current_index2 < len(text2):
                if text1[current_index1] == text2[current_index2]:
                    find = 1
                    tB1 = tBli1[current_index1]
                    tB2 = tBli2[current_index2]
                    for x in tB1:
                        self.list_tB[x].link = seq.list_tB[tB2[0]]
                    for x in tB2:
                        seq.list_tB[x].link = self.list_tB[tB1[0]]
                    text1 = text1[:current_index1]+text1[current_index1+1:]
                    tBli1 = tBli1[:current_index1]+tBli1[current_index1+1:]
                    text2 = text2[:current_index2]+text2[current_index2+1:]
                    tBli2 = tBli2[:current_index2]+tBli2[current_index2+1:]
                    current_index1 += -1
                    current_index2 += -1
            if find == 0:
                current_index1 = -1
        #for l in self.list_tB:
        #    print("in1:",l,l.text(),l.link)
        #for l in seq.list_tB:
        #    print("in2:",l,l.text(),l.link)


    def compare(self,seq):
        #compare the sequence with sequence seq
        #return 100 if identical
        #return a score < 100 if small differences
        text1 = " ".join([x.text() for x in self.list_tB])
        text2 = " ".join([x.text() for x in seq.list_tB])
        if text1 == text2:
            return 100
        text1 = text1.replace("- ","")
        text2 = text2.replace("- ","")
        #text1 = text1.replace("-","")
        #text2 = text2.replace("-","")
        if text1 == text2:
            return 95

        #find the percentage of same words
        n_total = 0
        n_same = 0
        
        li1 = text1.split()
        li2 = text2.split()
        n_total = min(len(li1),len(li2))+4
        ordering2 = []
        for i1 in li1[:]:
            for i2 in li2[:]:
                if i1 == i2:
                    ordering2.append(li2.index(i2))
                    li1.remove(i1)
                    li2.remove(i2)
                    break
        number_consecutive = 0
        for i in range(1,len(ordering2)):
            if ordering2[i-1] == ordering2[i]:
                number_consecutive += 1
        number_consecutive *= 1./(len(ordering2)+1)
        n_same = len(text1.split())-len(li1)
        if (n_total < 10) and (n_same *1. / n_total < 0.25):
            return 0
        if (n_same *1. / n_total < 0.5):
            return 0
        minnumletter = min(len(text1),len(text2))
        score = 90*number_consecutive*(n_same*1./n_total)
        return score


class Document:
    def __init__(self):
        self.filename = ""
        self.doc = None
        self.lines = {}
        self.sentences = {}
        self.pages_s = {}
        self.pages_l = {}
        self.page_list = []

    def set_nicerender(self):
        self.doc.setRenderHint(popplerqt4.Poppler.Document.TextAntialiasing)
        self.doc.setRenderHint(popplerqt4.Poppler.Document.Antialiasing)
        self.doc.setRenderHint(popplerqt4.Poppler.Document.TextHinting)
        self.doc.setRenderHint(popplerqt4.Poppler.Document.TextSlightHinting)
        self.doc.setRenderHint(popplerqt4.Poppler.Document.ThinLineSolid)

    def load(self,filename):
        self.filename = filename
        self.doc = popplerqt4.Poppler.Document.load(filename)

    def create_Lines(self,textBoxes,pagei):
        tBs = textBoxes[:]
        tBs.reverse()
        lines = []
        while tBs:
            tB = tBs.pop()
            x = tB.boundingBox().left()
            y = tB.boundingBox().top()
            if (x<70):
                continue
            if (y<70):
                continue
            if (y>760):
                continue
            newline = Sequence([pagei])
            tB.page = pagei
            newline.add(tB)
            tB2 = tB.nextWord()
            now = 0
            while tB2:
                now += 1
                tB2.page = pagei
                newline.add(tB2)
                if tB2 in tBs:
                    tBs.remove(tB2)
                tB2 = tB2.nextWord()
            if (newline.left() > 80 or newline.left() < 75) and (now <= 4):
                continue
            lines.append([newline.top(),newline])
        lines.sort()
        for l in range(len(lines)):
            self.lines["%04i_%04i"%(pagei,l)] = lines[l][1]
            self.add_line_on_page("%04i_%04i"%(pagei,l),pagei)

    def add_line_on_page(self,line_index,page_index):
        if page_index not in self.pages_l.keys():
            self.pages_l[page_index] = []
        if line_index not in self.pages_l[page_index]:
            self.pages_l[page_index].append(line_index)

    def add_sentence_on_page(self,sentence_index,page_index):
        if page_index not in self.pages_s.keys():
            self.pages_s[page_index] = []
        if sentence_index not in self.pages_s[page_index]:
            self.pages_s[page_index].append(sentence_index)

    def pagenumbers(self):
        if self.page_list == []:
            numPage = self.doc.numPages()
            self.page_list = range(numPage)
        #print("read pages (%i)"%numPage)
        #return range(20)
        #return [6]
        return self.page_list

    def run(self):
        self.lines = {}
        for i in self.pagenumbers():
            pdfPage = self.doc.page(i)
            textBoxes = pdfPage.textList()
            self.create_Lines(textBoxes,i)
        print("put all text together, by sentence")
        self.sentences = {}
        j = 0
        self.sentences[j] = Sequence()
        endofsentence = 0
        lk = list(self.lines.keys())
        lk.sort()
        for j1 in lk:
            l1 = self.lines[j1]
            for j2 in l1.list_tB:
                text = j2.text()
                if not text:
                    continue
                if text[-1] in ["!","?",".",";"]:
                    endofsentence = 1
                self.sentences[j].add(j2,j1)
                self.add_sentence_on_page(j,int(j1.split("_")[0]))
                if endofsentence:
                    endofsentence = 0
                    j += 1
                    self.sentences[j] = Sequence()
        print("number of sentences",len(self.sentences))
        #for i in self.sentences:
        #    print(self.sentences[i].text())

    def compare(self,doc):
        if self.sentences == {}:
            self.run()
        if doc.sentences == {}:
            doc.run()

        #reset the comparison_link
        for s1 in self.sentences:
            self.sentences[s1].comparison_link = None
            self.sentences[s1].comparison_score = 0
        for s2 in doc.sentences:
            doc.sentences[s2].comparison_link = None
            doc.sentences[s2].comparison_score = 0

        ##create comparison_link
        s1k = list(self.sentences.keys())
        s1k.sort()
        s2k = list(doc.sentences.keys())
        s2k.sort()

        print("comparison")
        matchings = {}
        for s1 in s1k[:]:
            ss1 = self.sentences[s1]
            scores = []
            for s2 in s2k:
                ss2 = doc.sentences[s2]
                score = ss1.compare(ss2)
                scores.append([score,-1*len(scores),s2])
                #so, during the sorting, the same score are sorted by occurence

                #if score == 100:
                #    break
                #I cannot break now, maybe the same
                # sentence will be used later.
            scores.sort(reverse=True)
            matchings[s1] = scores
            #print(s1,ss1.text(),scores)

        number_of_element = len(matchings)
        number_pairs = []
        while number_of_element > 0:
            find_max_item = None
            find_max_score = -1
            for k in matchings.keys():
                if len(matchings[k])==0:
                    continue
                while doc.sentences[matchings[k][0][2]].comparison_link != None:
                    matchings[k] = matchings[k][1:]
                    if len(matchings[k])==0:
                        number_of_element += -1
                        break
                if len(matchings[k])==0:
                    continue
                if find_max_score < matchings[k][0][0]:
                    find_max_score = matchings[k][0][0]
                    find_max_item = k
            if find_max_item == None:
                print("Uh ?",find_max_item,find_max_score,matchings)
                break
            if find_max_score == 0:
                break
            s2 = matchings[find_max_item][0][2]
            s1 = find_max_item
            self.sentences[s1].comparison_link = s2
            self.sentences[s1].comparison_score = find_max_score
            doc.sentences[s2].comparison_link = s1
            doc.sentences[s2].comparison_score = find_max_score
            number_pairs.append(find_max_score)
            self.sentences[s1].associate(doc.sentences[s2])
            del matchings[s1]
            number_of_element += -1
        print("pairs found:",len(number_pairs))
        #create links between textBoxes
            

    def read_Page(self,pagei):
        pdfPage = self.doc.page(pagei)
        return pdfPage

    def get_errorbox(self,page):
        tB_diff = []
        tB_same = []
        for k1 in self.pages_l.get(page,[]):
            for k2 in self.lines[k1].list_tB:
                if k2.link == None:
                    tB_diff.append(k2)
                else:
                    tB_same.append(k2)
        return tB_diff,tB_same

class Example(QtGui.QMainWindow):

    def __init__(self,app,argv):
        super(Example, self).__init__()
        self.app = app
        self.filename1 = "/home/caudron/Atlas/170417_note/0701_c/ttgamma_notes/intnote/intnote_ttgamma.pdf"
        self.filename2 = "/home/caudron/Atlas/170417_note/0703_c/ttgamma_notes/intnote/intnote_ttgamma.pdf"
        if len(argv) >= 3:
            self.filename1 = argv[1]
            self.filename2 = argv[2]
        self.images_doc1 = []
        self.images_doc2 = []
        self.labels_doc1 = []
        self.labels_doc2 = []
        self.floatinglabel_doc1 = None
        self.floatinglabel_doc2 = None
        self.total_height = 700
        self.initUI()

    def initUI(self):

        #display:
        #   ----------------------------------
        #  |   |   |        ||        |   |   |
        #  |   |   |        ||        |   |   |
        #  | 1 | 2 |   3a   ||   3b   | 4 | 5 |
        #  |   |   |        ||        |   |   |
        #  |   |   |        ||        |   |   |
        #   ----------------------------------
        #
        #  1: picture of map of the diff in doc1
        #  2: vertical scrollbar (move doc1 and doc2)
        #  3a: picture of pages of doc1 (width resizeable)
        #  3b: picture of pages of doc2 (width resizeable)
        #  4: vertical scrollbar (move doc2)
        #  5: picture of map of the diff in doc2

        self.setGeometry(0, 0, 1200, self.total_height)
        self.setWindowTitle('WhatPDF')

        self.main = QtGui.QWidget()
        self.setCentralWidget(self.main)

        self.scene_doc1 = QtGui.QGraphicsScene()
        self.view_doc1 = QtGui.QGraphicsView(self.scene_doc1,self.main)
        #self.view.setStyleSheet("background-color : gray; border:none;");
        #policy = self.view.sizePolicy()
        #policy.setHorizontalStretch(90)
        #self.view.setSizePolicy(policy)
        
        #self.scene_doc1.mousePressEvent = self.mousePressEvent_doc1
        #self.scene_doc1.mouseMoveEvent = self.mouseMoveEvent_doc1
        #self.scene_doc1.mouseReleaseEvent = self.mouseReleaseEvent_doc1
        #self.view_doc1.show()
        #self.view_doc1.setMouseTracking(True)


        self.scene_doc2 = QtGui.QGraphicsScene()
        self.view_doc2 = QtGui.QGraphicsView(self.scene_doc2,self.main)



        self.splitter = QtGui.QSplitter(self.main)
        self.splitter.addWidget(self.view_doc1)
        self.splitter.addWidget(self.view_doc2)

        self.scroll_doc1 = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.scroll_doc1_pos = 0
        self.scroll_doc1.setMaximum(500)
        self.scroll_doc1.connect(self.scroll_doc1, QtCore.SIGNAL("valueChanged(int)"), self.on_scroll_doc1_moved)
        self.map_doc1 = QtGui.QLabel("")
        self.mapimage_doc1 = QtGui.QImage(10,self.total_height,QtGui.QImage.Format_ARGB32)
        painter2 = QtGui.QPainter(self.mapimage_doc1)
        painter2.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 255)))
        painter2.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 255)))
        painter2.drawRect(QtCore.QRect(0,0,10,self.total_height))
        painter2.end()
        self.map_doc1.setFixedWidth(10)
        self.map_doc1.setFixedHeight(self.total_height)
        self.scroll_doc2 = QtGui.QScrollBar(QtCore.Qt.Vertical)
        self.scroll_doc2.setMaximum(500)
        self.scroll_doc2.connect(self.scroll_doc2, QtCore.SIGNAL("valueChanged(int)"), self.on_scroll_doc2_moved)
        self.map_doc2 = QtGui.QLabel("")
        self.mapimage_doc2 = QtGui.QImage(10,self.total_height,QtGui.QImage.Format_ARGB32)
        painter2 = QtGui.QPainter(self.mapimage_doc2)
        painter2.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 255)))
        painter2.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 255)))
        painter2.drawRect(QtCore.QRect(0,0,10,self.total_height))
        painter2.end()
        self.map_doc2.setFixedWidth(10)
        self.map_doc2.setFixedHeight(self.total_height)

        self.layout = QtGui.QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)
        self.layout.addWidget(self.map_doc1, 1)
        self.layout.addWidget(self.scroll_doc1, 2)
        self.layout.addWidget(self.splitter, 3)
        self.layout.addWidget(self.scroll_doc2, 4)
        self.layout.addWidget(self.map_doc2, 5)

        self.main.setLayout(self.layout)

        self.doc1 = Document()
        self.doc1.load(self.filename1)
        self.doc1.run()

        self.doc2 = Document()
        self.doc2.load(self.filename2)
        self.doc2.run()

        self.doc1.compare(self.doc2)

        self.scroll_doc1.setMaximum(100*len(self.doc1.pagenumbers()))
        self.scroll_doc2.setMaximum(100*len(self.doc2.pagenumbers()))

        self.doc1.set_nicerender()
        self.doc2.set_nicerender()

        self.boxes_doc1 = self.CreateImages(self.images_doc1,self.doc1,self.mapimage_doc1)
        self.boxes_doc2 = self.CreateImages(self.images_doc2,self.doc2,self.mapimage_doc2)

        self.map_doc1.setPixmap(QtGui.QPixmap(self.mapimage_doc1))
        self.map_doc2.setPixmap(QtGui.QPixmap(self.mapimage_doc2))
        self.map_doc1.im = self.mapimage_doc1
        self.map_doc2.im = self.mapimage_doc2

        self.hh = self.images_doc1[0].height()
        self.ww = self.images_doc1[0].width()

        for l in range(len(self.images_doc1)):
            self.labels_doc1.append(QtGui.QLabel("",self.view_doc1))
            self.labels_doc1[-1].setPixmap(QtGui.QPixmap(self.images_doc1[l]))
            y = self.hh*l
            self.labels_doc1[-1].move(0,y)
            self.labels_doc1[-1].mousePressEvent = self.mousePressEvent_doc1
            self.labels_doc1[-1].mouseMoveEvent = self.mouseMoveEvent_doc1
            #self.labels_doc1[-1].mouseReleaseEvent = self.mouseReleaseEvent_doc1
            self.labels_doc1[-1].setMouseTracking(True)

        self.floatinglabel_doc1 = QtGui.QLabel("",self.view_doc1)
        self.floatinglabel_doc1.move(-100,-100)
        self.floatinglabel_doc1.mousePressEvent = self.mousePressEvent_doc1

        for l in range(len(self.images_doc2)):
            self.labels_doc2.append(QtGui.QLabel("",self.view_doc2))
            self.labels_doc2[-1].setPixmap(QtGui.QPixmap(self.images_doc2[l]))
            self.labels_doc2[-1].mousePressEvent = self.mousePressEvent_doc2
            self.labels_doc2[-1].mouseMoveEvent = self.mouseMoveEvent_doc2
            self.labels_doc2[-1].setMouseTracking(True)
            y = self.hh*l
            self.labels_doc2[-1].move(0,y)

        self.floatinglabel_doc2 = QtGui.QLabel("",self.view_doc2)
        self.floatinglabel_doc2.move(-100,-100)
        self.floatinglabel_doc2.mousePressEvent = self.mousePressEvent_doc2

        self.show()

    def wheelEvent(self,event):

        self.floatinglabel_doc1.move(-100,-100)
        self.floatinglabel_doc2.move(-100,-100)

        pos1 = self.splitter.mapToGlobal(self.view_doc1.pos())
        pos2 = self.splitter.mapToGlobal(self.view_doc2.pos())
        delta = -15
        if event.delta() < 0:
            delta = 15
        if event.globalPos().x() > pos1.x() and event.globalPos().x() < pos1.x()+self.view_doc1.width():
            nsd2p = self.scroll_doc1.sliderPosition()+delta
            self.scroll_doc1.setSliderPosition(nsd2p)
        if event.globalPos().x() > pos2.x() and event.globalPos().x() < pos2.x()+self.view_doc2.width():
            nsd2p = self.scroll_doc2.sliderPosition()+delta
            self.scroll_doc2.setSliderPosition(nsd2p)


    def mouseMoveEvent_doc1(self,mouse):
        pos1 = self.splitter.mapToGlobal(self.view_doc1.pos())
        dy = self.labels_doc1[0].pos().y()
        real_x = (mouse.globalPos().x()-pos1.x())
        real_y = (mouse.globalPos().y()-pos1.y())+(-1*dy)
        relative_page_number = int(real_y*1./self.hh)
        real_page_number = self.doc1.pagenumbers()[relative_page_number]
        page_x = real_x
        page_y = real_y - (relative_page_number*self.hh)

        self.floatinglabel_doc1.move(-100,-100)
        self.floatinglabel_doc2.move(-100,-100)

        for l1 in self.boxes_doc1[real_page_number]:
            if l1[0].contains(page_x,page_y):
                if l1[1].link:
                    self.floatinglabel_doc1.l1 = l1[1]
                    self.DrawArrow(l1[1])
                break

    def mousePressEvent_doc1(self,mouse):
        if self.floatinglabel_doc1.pos().y() < -90:
            return
        for i in range(1000):
            if (self.floatinglabel_doc2.pos().y() >= self.floatinglabel_doc1.pos().y()):
                break
            nsd2p = self.scroll_doc2.sliderPosition()-2
            self.scroll_doc2.setSliderPosition(nsd2p)
            self.DrawArrow(self.floatinglabel_doc1.l1)
        for i in range(1000):
            if (self.floatinglabel_doc2.pos().y() <= self.floatinglabel_doc1.pos().y()):
                break
            nsd2p = self.scroll_doc2.sliderPosition()+2
            self.scroll_doc2.setSliderPosition(nsd2p)
            self.DrawArrow(self.floatinglabel_doc1.l1)

    def mouseMoveEvent_doc2(self,mouse):
        pos2 = self.splitter.mapToGlobal(self.view_doc2.pos())
        dy = self.labels_doc2[0].pos().y()
        real_x = (mouse.globalPos().x()-pos2.x())
        real_y = (mouse.globalPos().y()-pos2.y())+(-1*dy)
        relative_page_number = int(real_y*1./self.hh)
        real_page_number = self.doc2.pagenumbers()[relative_page_number]
        page_x = real_x
        page_y = real_y - (relative_page_number*self.hh)

        self.floatinglabel_doc1.move(-100,-100)
        self.floatinglabel_doc2.move(-100,-100)

        for l1 in self.boxes_doc2[real_page_number]:
            if l1[0].contains(page_x,page_y):
                if l1[1].link:
                    self.floatinglabel_doc1.l1 = l1[1].link
                    self.DrawArrow(l1[1].link)
                break

    def mousePressEvent_doc2(self,mouse):
        if self.floatinglabel_doc2.pos().y() < -90:
            return
        for i in range(1000):
            if (self.floatinglabel_doc1.pos().y() >= self.floatinglabel_doc2.pos().y()):
                break
            nsd2p = self.scroll_doc1.sliderPosition()-2
            self.scroll_doc1.setSliderPosition(nsd2p)
            nsd2p = self.scroll_doc2.sliderPosition()+2
            self.scroll_doc2.setSliderPosition(nsd2p)
            self.DrawArrow(self.floatinglabel_doc1.l1)
        for i in range(1000):
            if (self.floatinglabel_doc1.pos().y() <= self.floatinglabel_doc2.pos().y()):
                break
            nsd2p = self.scroll_doc1.sliderPosition()+2
            self.scroll_doc1.setSliderPosition(nsd2p)
            nsd2p = self.scroll_doc2.sliderPosition()-2
            self.scroll_doc2.setSliderPosition(nsd2p)
            self.DrawArrow(self.floatinglabel_doc1.l1)

    def DrawArrow(self,c):
        page1 = c.page
        page2 = c.link.page
        rect1 = self.Coord(c.boundingBox())
        rect2 = self.Coord(c.link.boundingBox())

        rpage1 = self.doc1.pagenumbers().index(page1)
        dy1 = self.labels_doc1[0].pos().y()
        dy1 += (rpage1*self.hh)
        rect1.moveTop(rect1.top()+dy1)

        image1 = QtGui.QImage(int(rect1.width()),int(rect1.height()),QtGui.QImage.Format_ARGB32)
        image1.fill(0)
        painter = QtGui.QPainter(image1)
        painter.setPen(QtGui.QPen(QtGui.QColor(255, 0, 0, 255)))
        painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 0)))
        painter.drawRect(0,0,int(rect1.width())-1,int(rect1.height())-1)
        painter.end()
        self.floatinglabel_doc1.move(rect1.left(),rect1.top())
        self.floatinglabel_doc1.setPixmap(QtGui.QPixmap(image1))
        self.floatinglabel_doc1.resize(int(rect1.width()),int(rect1.height()))

        rpage2 = self.doc2.pagenumbers().index(page2)
        dy2 = self.labels_doc2[0].pos().y()
        dy2 += (rpage2*self.hh)
        rect2.moveTop(rect2.top()+dy2)

        image2 = QtGui.QImage(int(rect2.width()),int(rect2.height()),QtGui.QImage.Format_ARGB32)
        image2.fill(0)
        painter = QtGui.QPainter(image2)
        painter.setPen(QtGui.QPen(QtGui.QColor(255, 0, 0, 255)))
        painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 0)))
        painter.drawRect(0,0,int(rect2.width())-1,int(rect2.height())-1)
        painter.end()
        self.floatinglabel_doc2.move(rect2.left(),rect2.top())
        self.floatinglabel_doc2.setPixmap(QtGui.QPixmap(image2))
        self.floatinglabel_doc2.resize(int(rect2.width()),int(rect2.height()))
        

    def on_scroll_doc1_moved(self,changed):
        previous = self.scroll_doc1_pos
        self.scroll_doc1_pos = changed
        dm = self.scroll_doc1.maximum()-self.scroll_doc1.minimum()
        fa = changed*1./dm
        hh = self.images_doc1[0].height()
        hy = int(-1*fa*hh*len(self.images_doc1))
        for l in self.labels_doc1:
            l.move(0,hy)
            hy += hh
        nsd2p = self.scroll_doc2.sliderPosition()+changed-previous
        self.scroll_doc2.setSliderPosition(nsd2p)
        #self.on_scroll_doc2_moved(nsd2p)


        mapimage = QtGui.QImage(self.mapimage_doc1)
        painter2 = QtGui.QPainter(mapimage)
        hy = int(-1*fa*hh*len(self.images_doc1))
        fa = (-1*hy)/(hh*(len(self.images_doc1)+1))
        dy = self.total_height*self.total_height*1./(hh*(len(self.images_doc1)+1))
        painter2.setPen(QtGui.QPen(QtGui.QColor(255, 255, 255, 255)))
        painter2.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 170)))
        painter2.drawRect(QtCore.QRect(0,int(self.total_height*fa),3,max(int(dy),3) ))
        painter2.end()
        self.map_doc1.setPixmap(QtGui.QPixmap(mapimage))


    def on_scroll_doc2_moved(self,changed):
        dm = self.scroll_doc2.maximum()-self.scroll_doc2.minimum()
        fa = changed*1./dm

        hh = self.images_doc2[0].height()
        hy = int(-1*fa*hh*len(self.images_doc2))
        for l in self.labels_doc2:
            l.move(0,hy)
            hy += hh

        mapimage = QtGui.QImage(self.mapimage_doc2)
        painter2 = QtGui.QPainter(mapimage)
        hy = int(-1*fa*hh*len(self.images_doc2))
        fa = (-1*hy)/(hh*(len(self.images_doc2)+1))
        dy = self.total_height*self.total_height*1./(hh*(len(self.images_doc2)+1))
        painter2.setPen(QtGui.QPen(QtGui.QColor(255, 255, 255, 255)))
        painter2.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 170)))
        painter2.drawRect(QtCore.QRect(6,int(self.total_height*fa),4,max(int(dy),3) ))
        painter2.end()
        self.map_doc2.setPixmap(QtGui.QPixmap(mapimage))

    def Coord(self,bb):
        x,y,w,h = bb.left()-1,bb.top()-1,bb.width()+2,bb.height()+1
        factor = 1.04
        return QtCore.QRectF((x*factor)+0,(y*factor)+0,w*factor,h*factor)

    def CreateImages(self,images,doc,mapimage):
        boxes = {}
        for pagei in doc.pagenumbers():
            pdfPage = doc.read_Page(pagei)
            image = pdfPage.renderToImage(75, 75, 0, 0, 600, 900)

            #painter = QtGui.QPainter(image)
            #painter.drawImage(QtCore.QPoint(600,0), image2)
            #pen = QtGui.QPen()
            #pen.setWidth(0)
            #pen.setColor(QtGui.QColor("red"))
            painter = QtGui.QPainter(image)
            painter.setPen(QtGui.QPen(QtGui.QColor(255, 230, 190, 255)))
            painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 230, 190, 255)))
            painter.setCompositionMode(QtGui.QPainter.CompositionMode_Multiply)


            diff,same = doc.get_errorbox(pagei)
            for c in diff:
                painter.drawRect(self.Coord(c.boundingBox()))

            painter.setPen(QtGui.QPen(QtGui.QColor(190, 255, 225, 255)))
            painter.setBrush(QtGui.QBrush(QtGui.QColor(190, 255, 225, 255)))
            if pagei not in boxes.keys():
                boxes[pagei] = []
            for c in same:
                rect = self.Coord(c.boundingBox())
                painter.drawRect(rect)
                boxes[pagei].append([rect,c])
            painter.end()

            images.append(image)

            painter2 = QtGui.QPainter(mapimage)
            dy = self.total_height*1./(len(doc.pagenumbers())+1)
            pi = doc.pagenumbers().index(pagei)
            ty = pi*dy
            painter2.setPen(QtGui.QPen(QtGui.QColor(75, 255, 190, 255)))
            painter2.setBrush(QtGui.QBrush(QtGui.QColor(75, 255, 190, 255)))
            for c in same:
                py = dy*(self.Coord(c.boundingBox()).y()*1./900)
                painter2.drawRect(QtCore.QRect(0,int(ty+py),10,1))
            painter2.setPen(QtGui.QPen(QtGui.QColor(255, 130, 90, 255)))
            painter2.setBrush(QtGui.QBrush(QtGui.QColor(255, 130, 90, 255)))
            for c in diff:
                py = dy*(self.Coord(c.boundingBox()).y()*1./900)
                painter2.drawRect(QtCore.QRect(0,int(ty+py),10,1))
            painter2.end()

        return boxes



def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example(app,sys.argv)
    sys.exit(app.exec_())

main()
